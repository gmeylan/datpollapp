var WebSocketServer = require("websocket").server;
var http = require("http");

var poll = { a: 9, b: 20, c: 30, d: 8, e: 12 };

function broadcastAll(message) {
  wsServer.connections.forEach(connection => {
    connection.sendUTF(JSON.stringify(message));
  });
}

var server = http.createServer(function(request, response) {
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(1337, function() {
  console.log("Listening mofo");
});
// create the server
wsServer = new WebSocketServer({
  httpServer: server
});

// WebSocket server
wsServer.on("request", function(request) {
  var connection = request.accept(null, request.origin);
  connection.sendUTF(JSON.stringify(poll));
  connection.on("message", function(message) {
    connection.sendUTF(JSON.stringify(message));
    if (message.type === "utf8") {
      poll.a = poll.a + 1;
      broadcastAll(poll);
    }
  });
});

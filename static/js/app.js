// set the dimensions and margins of the graph
var width = 450;
height = 450;
margin = 40;

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin;

var svg = {};

document.addEventListener("DOMContentLoaded", function(event) {
  // append the svg object to the div called 'my_dataviz'
  svg = d3
    .select("#myDiv")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  // Create dummy data
  var data = {};
});

var connection = new WebSocket("ws://localhost:1337");

connection.onmessage = function(message) {
  data = message.data;
  // Create dummy data
  var data = message.data;

  // set the color scale
  var color = d3
    .scaleOrdinal()
    .domain(data)
    .range(["white", "blue"]);

  // Compute the position of each group on the pie:
  var pie = d3.pie().value(function(d) {
    return d.value;
  });
  var data_ready = pie(d3.entries(data));

  svg.selectAll("*").remove();

  // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
  svg
    .selectAll("whatever")
    .data(data_ready)
    .enter()
    .append("path")
    .attr(
      "d",
      d3
        .arc()
        .innerRadius(0)
        .outerRadius(radius)
    )
    .attr("fill", function(d) {
      return color(d.data.key);
    })
    .attr("stroke", "black")
    .style("stroke-width", "2px")
    .style("opacity", 0.7);
};

function sendMessage() {
  connection.send("Hola");
}

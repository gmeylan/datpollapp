[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
# datpollapp

## How to run locally the project

Require:
 - Node v13.7.0 or higher
 - npm 6.13.7 or higher

### To run the project
- `npm install`
- `npm run server` to run backend server and Websocket server

### Linting
`npx eslint . --fix`

### TODO

- [ ] Create a persisting database with mongo
- [ ] Get a list of all the clients connected to broadcast all
- [ ] Dockerfile & docker-compose
- [ ] CI/CD pipeline
- [x] Linter setup